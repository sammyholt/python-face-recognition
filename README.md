# Python Face Recognition

A simple example of face recognition using Python.

## facematch.py
Determine if a known person is matched in another photo.

## findfaces.py
Find how many people are in an image.

## pullfaces.py
Show all faces in a given image.

## identify.py
Identify people in an image and show their name in a box around their face.